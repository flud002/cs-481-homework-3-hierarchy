﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Homework3_Hierarchy
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        
        public MainPage()
        {
            this.BackgroundImage = "italy1.jpg";
            InitializeComponent();
        }
        async void OnDrinkClicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new DrinkPage());
        }
        async void OnEntreeClicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new EntreePage());
        }
        async void OnDessertClicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new DessertPage());
        }
        void Handle_Appearing(object sender, EventArgs e)
        {


        }
        void Handle_Disappearing(object sender, EventArgs e)
        {


        }
    }
}
