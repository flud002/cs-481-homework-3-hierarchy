﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Homework3_Hierarchy
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EntreePage : ContentPage
    {
        public EntreePage()
        {
            InitializeComponent();
        }
        async void OnDessertClicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new DessertPage());
        }
        async void Handle_Appearing(object sender, EventArgs e)
        {
            await Task.Delay(200);
            this.BackgroundColor = Color.Orange;
        }
        async void Handle_Disappearing(object sender, EventArgs e)
        {
            await Task.Delay(200);
            this.BackgroundColor = Color.Yellow;
        }
    }
}