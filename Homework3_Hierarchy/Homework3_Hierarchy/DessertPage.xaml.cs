﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Homework3_Hierarchy
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DessertPage : ContentPage
    {
        public DessertPage()
        {
            InitializeComponent();
        }
        async void OnDrinkClicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new DrinkPage());
        }
        async void OnMenuClicked(object sender, EventArgs e)
        {
            await Navigation.PopToRootAsync();
        }
        async void Handle_Appearing(object sender, EventArgs e)
        {
            await Task.Delay(200);
            this.BackgroundColor = Color.Pink;
        }
        async void Handle_Disappearing(object sender, EventArgs e)
        {
            await Task.Delay(200);
            this.BackgroundColor = Color.Purple;
        }
    }   
}