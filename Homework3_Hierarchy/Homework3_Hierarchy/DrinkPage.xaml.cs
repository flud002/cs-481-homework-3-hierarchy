﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Homework3_Hierarchy
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DrinkPage : ContentPage
    {
        public DrinkPage()
        {
            InitializeComponent();
        }
        async void OnEntreeClicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new EntreePage());
        }
        async void Handle_Appearing(object sender, EventArgs e)
        {
            await Task.Delay(200);
            this.BackgroundColor = Color.SkyBlue;
        }
        async void Handle_Disappearing(object sender, EventArgs e)
        {
            await Task.Delay(200);
            this.BackgroundColor = Color.LightCoral;
        }
    }
}